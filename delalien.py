#%% Importing libraries
import pandas as pd
import numpy as np

import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from collections import Counter

#%% Loading the dataset

train = pd.read_csv("data/train.csv")
test = pd.read_csv("data/test.csv")

y_true = train['score']

X = pd.concat([train.iloc[:,:-1], test], axis = 0)
#%%

# le_df = pd.DataFrame()

# num_df = pd.DataFrame()

# for column in train.columns:
#     if train[column].dtype == type(object):
#         le = LabelEncoder()
#         le_df[column] = le.fit_transform(train[column])
#     else:
#         num_df[column] = train[column]

# df = pd.concat([le_df, num_df], axis = 1)
        
#%%
# =============================================================================
# 
# cat_df = pd.DataFrame()
# 
# num_df = pd.DataFrame()
# 
# for column in train.columns:
#     if train[column].dtype == type(object):
#         cat_df[column] = train[column]
#     else:
#         num_df[column] = train[column]
# =============================================================================
#%% Create dummy variables for cat_vars

cat_vars = ['earthling_country', 'period_of_stay', 'earthling_type', 'swimming_pool', 'exercise_room',
            'basketball_court', 'yoga_classes', 'club', 'free_wifi', 'hotel_name', 'earthling_continent',
            'review_month', 'review_weekday']

#df = pd.get_dummies(df, columns=['type'])
cat_df = X[cat_vars]
cat_dummy_df = pd.get_dummies(cat_df, columns = cat_vars, drop_first = True)

num_vars = ['total_reviews', 'hotel_reviews', 'helpful_votes', 'number_of_rooms','mars_membership_years']
num_df = X[num_vars]

final_df = pd.concat([cat_dummy_df, num_df], axis = 1)


#%% Split it back to train and test csv dataframes

train_df = final_df.iloc[:len(train.score),:]

test_df = final_df.iloc[len(train.score):,:]

final_df = pd.concat([train_df, y_true], axis = 1)
#%%
from imblearn.over_sampling import RandomOverSampler

print('Original dataset shape {}'.format(Counter(final_df.iloc[:,-1])))
ros = RandomOverSampler(random_state=42)
dfx, dfy = ros.fit_sample(final_df.iloc[:,:-1], final_df.iloc[:,-1])
print('Resampled dataset shape {}'.format(Counter(dfy)))


#%% train test split

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(dfx, dfy, test_size = 0.3, random_state=0)

#%% Modeling - Log Regression

from sklearn.linear_model import LogisticRegression

logreg = LogisticRegression()
logreg.fit(X_train,y_train)

y_pred=logreg.predict(X_test)

#%% Random Forest

from sklearn.ensemble import RandomForestClassifier

rfc = RandomForestClassifier()
rfc.fit(X_train,y_train)

y_pred = rfc.predict(X_test)

from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_pred)

#%% Confusion matrix 
from sklearn import metrics
cnf_matrix = metrics.confusion_matrix(y_test, y_pred)
cnf_matrix

#%% Accuracy Score
from sklearn.metrics import accuracy_score
accuracy_score(y_test, y_pred)


#%% Predict on the test set


#%%
y_pred = rfc.predict(test_df)

#%% Export to csv
# x = pd.Series(y_pred)
# x.to_csv("df_2.csv", index = False)


